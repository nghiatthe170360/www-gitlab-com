---
layout: handbook-page-toc
title: 'Create:IDE Team'
description: >-
  The IDE Team is part of the Create Stage. We focus on multiple categories:
  Remote Development, Web IDE and GitLab VS Code Extension.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The team is part of [Create Stage][hb-create] in the [Dev Sub-department][hb-dev]. We focus on multiple [categories][hb-categories]: `Remote Development`, `Web IDE`, and the `GitLab VS Code Extension`.

### 👌 Team OKRs

If you're interested in the team's Objectives and Key Results (OKRs), you can find them on [GitLab](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=title_asc&state=opened&label_name%5B%5D=group%3A%3Aeditor&first_page_size=20).

### 🤴 Team Principles

[Create:IDE Principles][principles]: What Are the Create:IDE Team Principles?

### 🚀 Team Members

The following people are permanent members of the IDE Engineering Team:

**Engineering Manager & Engineers**

<%= direct_team(manager_slug: 'davidoregan') %>

**Product, Design & Quality**

<%= stable_counterparts(role_regexp: /Create:IDE/, direct_manager_role: 'Engineering Manager, Create:IDE') %>

### ☕ Team Category DRIs

| Category | DRI |
| ---- | ------- |
| Remote Development | Vishal Tak |
| Web IDE  | Paul Slaughter |
| GitLab VS Code Extension | Tomas Vik |

### ☎️ How to reach us

Depending on the context here are the most appropriate ways to reach out to the IDE Group:

- GitLab epics/issue/MRs: `@gl-editor`
- Slack: [`#g_create_editor`][slack] and `@create-editor-team`

### 📆 Team Meetings

**❗️Important**: For every meeting, the [IDE team's meeting document][gdoc] should be used, and filled with the meeting notes, as well as references to any other sync meeting agendas/notes/recordings which have recently occurred. This will make it easier for people to find any meeting notes.

These are regular meetings in which most team members will participate. Below is our schedule:

#### Iteration Planning Meeting (IPM)

- **When:** Every Wednesday, alternating between APAC and EMEA time zones.
- **What:** In this weekly meeting, we will review the team's backlog and the current iteration status. We'll estimate and prioritize work for the next one or two iterations. The meeting will alternate between APAC and EMEA time zones to accommodate different regions.

#### Pre-Iteration Planning Meeting

- **When:** Every Wednesday, alternating between APAC and EMEA time zones.
- **What:** The goal of the Pre-IPM meeting is for the team leaders to collaborate on preparing all issues which are to be prioritized for the upcoming iteration(s), so they are ready for the wider team to discuss and estimate in the next IPM. For more information, see ["Pre-IPM Process"](#-pre-ipm-process).

#### Quarterly Strategy and OKR Planning Meeting

- **When:** Once every quarter.
- **What:** In this meeting, we will discuss and align our team's strategic goals and objectives. We'll go over our progress on OKRs and define the next steps to achieve them.

#### Think Big Sessions / Optional Sync Call

- **When:** Once a month, on the week when there is no Quarterly Strategy Meeting.
- **What:** We can use this time for "Think Big" sessions or other optional synchronous calls.

#### Open Office Hours Meeting

- **When:** Every other week, alternating between APAC and AMER friendly times.
- **What:** This meeting is an open agenda, and if there are no topics, we will cancel the meeting. It's a chance to drop by and have an informal chat with colleagues. The meeting will alternate between APAC and AMER friendly times to cover different regions.

#### Retro Meeting

- **When:** Every fourth Thursday of the month.
- **What:** In this monthly meeting, we'll go over the feedback from our asynchronous team retro and identify action items and next steps. Our goal is to improve team efficiency by learning from our past mistakes and successes.

## 📦 Team Workflow And Processes

### 🖖 Weekly EM Updates

Each week the team EM provides a Weekly Status update issue which aims to capture the most important items for the team to be aware of. These can be found [here](https://gitlab.com/gitlab-com/create-stage/editor/-/issues/?sort=title_asc&state=all&label_name%5B%5D=Weekly%20Team%20Announcements&first_page_size=20).

### Milestone Planning And Remote Development Iteration Planning

Currently, our team has two primary planning processes: one for Remote Development and another for the Web IDE and GitLab VS Code Extension. 

We have adopted this approach due to the nature of Remote Development being a large greenfield feature, with many engineering-centric infrastructural and technical concerns and unknowns which will continue to evolve and change while it is still in the early stages of maturity. Therefore, it makes sense for the planning to be a closely collaborative process between Product and Engineering, with tight feedback loops, and less process and ceremony around when we need to change plans/priorities.

To address this issue, we have decided to modify parts of the [Plan](https://about.gitlab.com/handbook/product-development-flow/#build-phase-1-plan) and [Build & Test](https://about.gitlab.com/handbook/product-development-flow/#build-phase-2-develop--test) phases of the GitLab Product Development Flow to a more lightweight process inspired by [XP](https://www.amazon.com/Extreme-Programming-Explained-Embrace-Change/dp/0321278658) and [Scrum](https://www.scrum.org/resources/blog/agile-metrics-velocity).

This offers greater support for the tight feedback loops and close collaboration between Product and Engineering, which will be critical to the execution and success of the Remote Development project.

We will implement this by dogfooding the [Iterations feature](https://docs.gitlab.com/ee/user/group/iterations/index.html) of GitLab, and following the [documented process for running agile iterations](https://docs.gitlab.com/ee/tutorials/agile_sprint.html) as closely as is practical.

The Web IDE and GitLab VS Code Extension features do not have these same concerns, therefore they will still use the standard GitLab Product Development Flow.

#### 📄 Milestone Planning 

Every month, we create a Milestone Planning issue that serves as the single source of truth (SSoT) for the current milestone.

This issue provides us with a centralized location to discuss our upcoming milestone work, plan for feature, bug, and maintenance tasks, and provides us with the necessary flexibility to iterate mid-milestone if required.

Populate the milestone planning issue with issues from the prioritized boards placed in the ~"workflow::ready for development" column. Feel free to add maintenance issues that aren’t directly related to the Editor group as well.

Move the issues assigned to the current milestone to the ~"workflow:: in dev" column. Also, assign the ~"Deliverable" label and assign the target milestone.

You can find all milestone planning issues in the [IDE team’s project](https://gitlab.com/gitlab-com/create-stage/editor/-/issues/?sort=title_asc&state=all&label_name%5B%5D=Planning%20Issue&first_page_size=20).

The layout for these Milestone issues are automated and can be found [here](https://gitlab.com/gitlab-com/create-stage/editor/-/blob/main/.gitlab/issue_templates/planning_issue.md).

#### 🤖 Remote Development Iteration Planning

Remote Development Iteration Planning is a weekly process where a team reviews the backlog and the current iteration status, estimates and prioritizes work for the next iteration, and uses merge requests (MRs) as the single source of truth for discussions and progress.

The iteration cycle is one week long, and the team maintains one board for tracking their progress - [the Iteration Planning board](https://gitlab.com/groups/gitlab-org/-/boards/5283620?label_name[]=Deliverable&label_name[]=Category%3ARemote%20Development).

The board's workflow involves:

- Issues with the `~Deliverable` and `~Category:Remote Development` labels and an assigned iteration are currently being worked on and should have a weight and a one-to-one relationship with an MR.
- Issues with the `~Deliverable` and `~Category:Remote Development` labels but without an assigned iteration are the next up in terms of work and are in the Open lane and should have a weight and a one-to-one relationship with an MR.
- Anything without the `~Deliverable` and `~Category:Remote Development` labels is considered part of the backlog and needs to be reviewed before it can be picked up.

The team updates the board before each iteration meeting and holds weekly or bi-weekly pre-IPM meetings to synchronize and populate the Open lane with issues that could be potential candidates for the next iteration.

#### 🤖 Web IDE / VS Code milestone planning

The Web IDE and GitLab VS Code Extension still use the default milestone planning process which is loosely based in the [Plan](https://about.gitlab.com/handbook/product-development-flow/#build-phase-1-plan) and 
[Develop & Test](https://about.gitlab.com/handbook/product-development-flow/#build-phase-2-develop--test) phases 
of the product development workflow. We recommend reading these references for a better understanding of this workflow.

These categories have prioritized boards that are populated during the 
[Bi-Weekly Asynchronous Backlog Refinement Process](#-bi-weekly-asynchronous-backlog-refinement-process):

- [Web IDE Prioritized Board](https://gitlab.com/groups/gitlab-org/-/boards/5288010?milestone_title=Upcoming&label_name[]=Category%3AWeb%20IDE&label_name[]=Deliverable)
- [VS Code Prioritized Board](https://gitlab.com/groups/gitlab-org/-/boards/5501205?milestone_title=Upcoming&label_name[]=Deliverable&label_name[]=VS%20Code)

Each column on the board represents a phase of the development workflow:

- The open lane is populated with issues labeled with the `~Category:[Category]` and `~Deliverable` labels. These labels signal that the issue is deemed a priority by the team. 
- `workflow::ready for development` contains issues that are ready to pick in an upcoming milestone. 
- `workflow::refinement` contains issues that require more investigation or to break them down into smaller units of work that can be delivered in a single milestone.
- `worflow::in dev` contains ongoing work. 
- `workflow::complete` contains completed work.  

The following diagram visualizes the milestone planning and delivery process:

``` mermaid
graph

classDef workflowLabel fill:#428BCA,color:#fff;
classDef issueCreateLabel fill:#6aa84f,color:#fff;

  A(Issue Created):::issueCreateLabel
  B(WebIDE or VSCode Prioritized):::workflowLabel
  C{Issue needs refinement?}:::workflowLabel
  D(workflow::refinement):::workflowLabel
  E(workflow::ready for development):::workflowLabel
  F(workflow::in dev):::workflowLabel
  G(workflow::complete):::workflowLabel

  A -- Reviewed by EM, PM, Design, and end leads --> B 
  B --> C
  C -- Yes --> D
  D -- tech spike, break down issue, etc. --> C
  C -- No --> E
  E -- Schedule for current milestone --> F
  F -- Approved and merge --> G
  G --> CLOSE
```

#### 🏋 What Weights to use

To assign weights to issues effectively, it's important to remember that issue weight should not be tied to time. Instead, it should be a purely abstract measure of the issue's significance. To accomplish this, the our team uses the Fibonacci sequence starting from weight 0:

- **Weight 0:** Reserved for the smallest and easiest issues, such as typos or minor formatting changes, or very minor code changes with no tests required.
- **Weight 1:** For simple issues with little or no uncertainty, risk or complexity. These issues may have labels like "good for new contributors" or "Hackathon - Candidate.". For example:
    - Changing copy text which may be simple but take some time.
    - Making CSS or UI adjustments.
    - Minor code changes to one or two files, which require tests to be written or updated.
- **Weight 2:** For more involved issues which are still straightforward without much risk or complexity, but may involve touching multiple areas of the code, and updating multiple tests.
- **Weight 3:** For larger issues which may have some unforeseen complexity or risk, or require more extensive changes, but is still not large enough to warrant  [breaking down into smaller separate issues](#-breaking-down-large-issues).
- **Weight 5:** Normally, this weight should be avoided, and indicate that the issue ideally [should be broken down into smaller separate issues](#-breaking-down-large-issues). However, in some cases a issue with a weight of 5 might still be prioritized. For example, if there is a large amount of manual updates to be made which will require a large amount of effort, but doesn't necessarily involve significant risk or uncertainty.
- **Weight 8/13+:** Weights above 5 are used to clearly indicate work that is not yet ready to be assigned for implementation, and _must_ be broken down because it is too large in scope to start implementing, and/or still has too many unknowns/risks. This weight is temporarily assigned to "placeholder" issues to capture the scope of the effort in our velocity-based capacity planning calculations. For more information, see ["Breaking Down Large Issues"](#-breaking-down-large-issues).

#### 📝 Breaking Down Large Issues

Some issues are large in scope to start implementing, and/or still has too many unknowns/risks. In this case, we should break it down into smaller issues which can be implemented in a single iteration (with a weight of 5 or less, ideally 3 or less). Here's our process:

1. Create an investigation issue to identify the work that needs to be done. This issue represents the work needed to research, investigate, discover and document the effort, and break the work down into new issues which are small and clear enough to be prioritized and started. The investigation issue should have a weight assigned which reflects the effort required perform this investigation and breakdown. Here's an example: https://gitlab.com/gitlab-org/gitlab/-/issues/408186.
2. While the investigation issue work is ongoing, we create a temporary "placeholder" issue on our Iteration Board. This placeholder issue serves to capture the scope of the effort in our velocity-based capacity planning calculations while the investigation to break is down is still ongoing.
    1. It should have a weight of either 8/13+ (indicating that it is too large to be prioritized and started, and must be broken down.
    1. The title format for the placeholder issue is `[Category] - Iteration Planning Placeholder for [description of work]`. Here's an example: https://gitlab.com/gitlab-org/gitlab/-/issues/408093.
    1. The description should have a link to the investigation issue, and a reminder to remove the weight and close it once the smaller replacement issues are created.
    1. The `~blocked` label can be applied so it is clear that this issue should is not ready for development. Unfortunately, we must still apply the `~Deliverable` label to this issue, because that is the only way we can associate it with the Iteration Cadence for velocity calculations.
3. Once the investigation and breakdown into smaller issues is complete, then the weight can be removed and the issue closed.

#### 📝 Pre-IPM Process

The Pre-Iteration Planning Meeting is a crucial step that takes place every Wednesday, alternating between APAC and EMEA time zones, to prepare for the weekly Iteration Planning Meeting (IPM).

During the Pre-IPM meeting, the leaders on the team will collaborate on creating, refining, organizing, and clarifying all issues which are to be prioritized for the upcoming iteration(s). This will normally involve Product and Engineering leaders on the team, but may also involve Design or other team members depending on the nature of the issues involved.

The goal of the Pre-IPM is to ensure that all issues which are to be prioritized in the upcoming iteration(s) are ready for the wider team to briefly discuss and estimate in the next IPM.

The goal of this meeting is to ensure that the backlog is well-defined, prioritized, and that each issue is estimable and deliverable within a single iteration. The team works together to size each story and determine the iteration capacity. This helps us commit to completing the work within the upcoming iteration(s).

The outcome of the Pre-IPM process is a set of curated issues that represent the work to be done. These issues will be used to populate the team's Iteration board. For each category, a placeholder issue will be created, following a specific title format. This placeholder issue will be used during the IPM to track progress and ensure that the team stays on track.

### 😷 Issue Workflow Hygiene

In the Create:IDE team we leverage an automatic issue hygiene system via the [triage bot](https://gitlab.com/gitlab-org/quality/triage-ops/-/tree/master/policies/groups/gitlab-org/editor). This helps to ensure issues and label hygiene are respected, currently, our rules are:

- TODO: Update our rules once the team renaming is complete. 

### 🌉 Planning Guidelines

We use the terms _Architecture Plan_ and _Iteration Plan_ when we think of outcomes to broad high-level issues. An investigative spike should result in an Architecture Plan and an Iteration Plan.

- **Architecture Plan**: A high-level vision of a technical approach that is shown to solve user problems. This plan includes a formulation of specific [quality attributes](https://en.wikipedia.org/wiki/List_of_system_quality_attributes) that are important for this use cases (such as performance, usability, or security). It also includes an outline of technical approaches that will satisfy these quality attributes in addition to the functional requirement. A [spike effort](https://en.wikipedia.org/wiki/Spike_(software_development)) should be created to verify and explore the technical approach for an architecture plan. The spike could result in new architectural concerns, resulting in an iteration of the plan.
- **Iteration Plan**: A plan for how we'll iteratively implement an Architecture Plan or another objective. This can be composed of low-level technical steps, or medium-level slices of use cases. The iteration plan should result in a set of issues (and possible epics) with clearly defined scope and weights.

**Sometimes it takes a time to develop a well fleshed-out iteration plan. In these cases, A "Formulate Iteration Plan" weighted issue can be used.**

### 📝 Issue Guidelines

These guidelines apply to all issues we use for planning and scheduling work within our group. Our Engineers can define specific implementation issues when needed, but the overall goal for our issues are as follows:

- Treat the wider community as the primary audience ([see relevant summary for rationale][community-contributions-wider-community]).
- Provide a meaningful **title** that describes a deliverable result.
    - ✅ `Add a cancel button to the edit workspace form page`
    - ✅ `Automatically save Devfile changes after 2 seconds of inactivity`
    - ❌ `Make WebIDE better`
- Provide a meaningful description that clearly explains the goal of the issue, and provide some technical details if necessary.
- Should there be critical implementation steps or other useful ways to create small tasks as part of the issue, please use a checklist as part of the issue descriptions?
- The issue should have a weight assigned

It's okay to create specific engineering-driven implementation issues for more complex features. These would be called **Child Issues** and they should always link back to their parent. If one issue would spawn many child issues, consider creating an Epic.

### ⏲ Time Off

Team members should add any [planned time off](/handbook/paid-time-off/#paid-time-off) in the ["Time Off by Deel"](https://gitlab.slack.com/archives/D019WTM2F99) slack app, so that the Engineering Manager can use the proper number of days off during capacity planning.

## 👏 Communication

The IDE Team communicates based on the following guidelines:

1. Always prefer async communication over sync meetings.
1. Don't shy away from arranging a [sync call](#ad-hoc-sync-calls) when async is proving inefficient, however always record it to share with team members.
1. By default communicate in the open.
1. All work-related communication in Slack happens in the [#g_create_editor][slack] channel.

### 🤙 Ad-hoc sync calls

We operate using async communication by default. There are times when a sync discussion can be beneficial and we encourage team members to schedule sync calls with the required team members as needed.

## 🔗 Other Useful Links

### 🏁 Developer Cheatsheet

[Developer Cheatsheet][cheatsheet]: This is a collection of various tips, tricks, and reminders which may be useful to engineers on (and outside of) the team.

### 🤗 Fostering Wider Community Contributors

We want to make sure that all the fields of the Create:IDE team are approachable for outside contributors.
In this case, if issues should be good for any contribution it should be treated with extra care. Therefore have a look at this excellent guide written by our own Paul Slaughter!

[Cultivating Contributions from the Wider Community][community-contributions]: This is a summary of why and how we cultivate contributions from the wider community.

### 📹 GitLab Unfiltered Playlist

The IDE Group collates all video recordings related to the group and its team members in [a playlist][youtube] in the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) YouTube channel.

<%= partial "handbook/engineering/metrics/partials/_cross_functional_dashboard.erb", locals: { filter_value: "IDE" } %>

<!-- LINKS START -->

[hb-create]: /handbook/engineering/development/dev/create/
[hb-dev]: /handbook/engineering/development/dev/
[hb-categories]: https://about.gitlab.com/direction/create/#categories-in-create
[product-development-flow]: /handbook/product-development-flow/
[workflow-labels]: https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#labels
[gdoc]: https://docs.google.com/document/d/1b-dgL0ElBf_I3pbBUFISTYBG9VN02F1b3TERkAJwJ20/edit#
[slack]: https://gitlab.slack.com/archives/CJS40SLJE
[youtube]: https://www.youtube.com/playlist?list=PL05JrBw4t0KrRQhnSYRNh1s1mEUypx67-
[cheatsheet]: /handbook/engineering/development/dev/create/ide/developer-cheatsheet/
[principles]: /handbook/engineering/development/dev/create/ide/principles/
[community-contributions]: /handbook/engineering/development/dev/create/ide/community-contributions/
[community-contributions-wider-community]: /handbook/engineering/development/dev/create/ide/community-contributions/#wider-community-as-primary-audience

<!-- LINKS END -->
